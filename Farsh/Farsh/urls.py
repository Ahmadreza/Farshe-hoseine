"""Farsh URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.contrib import admin
from app.views import *
from Farsh import settings
from django.conf.urls.static import static
from django.contrib.auth import views as auth_views
from fa.views import *
urlpatterns = [

    url(r'^accounts/logout/$', auth_views.LogoutView.as_view(), name='logout'),
    url(r'^accounts/login/', auth_views.LoginView.as_view(template_name="login.html"), name="login"),
    url(r'^accounts/', include('django.contrib.auth.urls')),

    url(r'^admin/', admin.site.urls, name="admin"),
    url(r'^cart/$', sabad_kharid, name="cart"),

    url(r'^products/$', products, name="products"),
    url(r'^products/(?P<query>\w+)/$', products1, name="products"),
    url(r'^$', homepage, name="home"),
    url(r'^about_us', about_us, name="about_us"),

    url(r'^contact/$', contact_us, name="contact"),
    url(r'^signup/$', signup, name="signup"),

    url(r'^complaint/$', complaint, name="complaint"),
    url(r'^factor/$', factor, name="factor"),

    url(r'^order_registration/(?P<bag_id>\d+)/$', sabte_sefaresh, name="order_registration"),
    url(r'^update_address/(?P<bag_id>\d+)/$', update_address, name="update_address"),
    url(r'^^product_page/carpet/(?P<product_id>\d+)/$', product_page, name="product_page"),

    url(r'^search_result/$', search_result, name="search_result"),

    url(r'^order_track/$', peygiri_sefaresh, name="order_track"),
    url(r'^order/$', order, name="order"),

    url(r'^help/$', help, name="help"),
    url(r'^404/$', not_found, name="not_found"),
    url(r'^blog/post/(?P<post_id>[0-9]+)/?$', in_blog, name="in_blog"),
    url(r'^blog/$', blog, name="blog"),
    url(r'^order/remove/(?P<item_id>\d+)/$', remove_item, name="remove_item"),
    url(r'^profile/$', profile, name="profile"),
    url(r'^after_sabte_sefaresh/(?P<bag_id>\d+)/$', after_sabte_sefaresh, name="after_sabte_sefaresh"),

    # administratives #
    url(r'^administrator/$',administration,name='administratorhome'),
    url(r'^administrator/carpets/$', admin_carpets_index, name="admin_carpets"),
    url(r'^administrator/complaints/$', admin_complaints_index, name="admin_carpets"),
    url(r'^administrator/contact/$', admin_contacts_index, name="admin_carpets"),
    url(r'^administrator/carpet/create/$', admin_carpet_create,name='carpet_create'),
    url(r'^administrator/carpet/api/?$', Carpets.as_view()),
    url(r'^administrator/contact/api/?$', Contacts.as_view()),
    url(r'^administrator/complaint/api/?$', Complaints.as_view()),
    url(r'^administrator/carpet/api/(?P<carpet_id>[0-9]+)/?$', Carpets.as_view()),
    url(r'^administrator/contact/api/(?P<contact_id>[0-9]+)/?$', Contacts.as_view()),
    url(r'^administrator/complaint/api/(?P<complaint_id>[0-9]+)/?$', Complaints.as_view()),
    url(r'^administrator/carpet/update/(?P<pk>\d+)/$', admin_carpet_update, name='carpet_update'),
    url(r'^api/get_carpet_categories/', get_carpet_categories, name='get_carpet_categories'),
    url(r'^api/get_carpets/', get_carpets, name='get_carpets'),
    url(r'^administrator/carpet_categories/$',admin_carpet_categories_index),
    url(r'^administrator/carpet_category/create/$', admin_carpet_category_create,name='carpet_category_create'),
    url(r'^administrator/carpet_category/api/?$', CarpetCategories.as_view()),
    url(r'^administrator/carpet_category/api/(?P<category_id>[0-9]+)/?$', CarpetCategories.as_view()),
    url(r'^administrator/carpet_category/update/(?P<pk>\d+)/$', admin_carpet_category_update, name='carpet_category_update'),
    url(r'^administrator/user/$',admin_user_index),
    url(r'^administrator/user/create/$', register_admin,name='user_create'),
    url(r'^administrator/user/update/(?P<pk>\d+)/$', admin_user_update, name='user_update'),
    url(r'^administrator/user/api/?$', Users.as_view()),
    url(r'^administrator/user/api/(?P<user_id>[0-9]+)/?$', Users.as_view()),
    url(r'^fa/administrator/complaint/quick/answer/(?P<pk>\d+)/$', fa_admin_complaint_answer_quick_create, name='complaint_answer_quick'),
    url(r'^fa/administrator/text/complaint/(?P<pk>\d+)/$', text_complaint, name='text_complaint'),
    url(r'^fa/administrator/contact/quick/answer/(?P<pk>\d+)/$', fa_admin_contact_answer_quick_create, name='contact_answer_quick'),
    url(r'^fa/administrator/text/contact/(?P<pk>\d+)/$', text_contact, name='text_contact'),
    url(r'^administrator/post/$',fa_admin_post_index),
    url(r'^administrator/post/create/$', fa_admin_post_create,name='post_create'),
    url(r'^administrator/post/api/?$', Posts.as_view()),
    url(r'^administrator/post/api/(?P<post_id>[0-9]+)/?$', Posts.as_view()),
    url(r'^administrator/post/update/(?P<pk>\d+)/$', fa_admin_post_update, name='post_update'),
]
urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
