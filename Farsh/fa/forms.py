from django import forms
from crispy_forms.helper import FormHelper
from app.models import Carpet, CarpetCategory,User, Profile, ComplaintAnswer, ContactAnswer,Post
from crispy_forms.layout import Layout, Div, HTML
from django.utils.translation import ugettext_lazy as _
from django.forms.utils import ErrorList


class CarpetForm(forms.ModelForm):
    helper = FormHelper()
    helper.form_id = "carpetform"
    helper.form_method = "post"
    helper.form_class = "js-upload-carpet-form"
    color = forms.MultipleChoiceField(choices=(
    ('ندارد','ندارد'),
    ('زرد','زرد'),
    ('آبی','آبی'),
    ('قرمز','قرمز'),
    ('بنفش','بنفش'),
    ('سبز','سبز'),
    ('نارنجی','نارنجی'),
    ('سفید','سفید'),
    ('مشکی','مشکی'),
    ('صورتی','صورتی'),
    ('قهوه ای','قهوه ای'),
    ),label="رنگ")
    helper.layout = Layout(
        Div(HTML('<div class=panel-heading><p class="panel-title">مشخصات اولیه</p></div>'),

            Div('title','category','description'
                    ,css_class="panel-body")
                    ,css_class="panel panel-warning"),
         Div(HTML('<div class=panel-heading><p class="panel-title">تصاویر و ویژگی ها</p></div>'),
        HTML('<ul class="nav nav-tabs"><li class="active"><a data-toggle="tab" href="#tabmenu1">گالری تصاویر</a></li><li><a data-toggle="tab" href="#tabmenu2">ویژگی ها</a></li></ul>'),
        Div(
            Div('image1', 'image2','image3', 'image4', 'image5','image6',
        css_id="tabmenu1",css_class="tab-pane fade in active"),
        Div(
            'brush_num',
                'density',
                'colors_num',
                'yarn_type',
                'wool_yarn_type',
                'yarn_height',
                'antipilling',
                'weight',
                'guarantee_len',
                'delivery_time',
                'color',
        css_id="tabmenu2", css_class="tab-pane fade")

        ,css_class="tab-content"),
             css_class="panel panel-warning"),
        Div(HTML('<div class=panel-heading><p class="panel-title">سایز و قیمت</p></div>'),
HTML('<ul class="nav nav-tabs">'
     '<li class="active"><a data-toggle="tab" href="#tabmenu3">سایز 1</a></li>'
     '<li><a data-toggle="tab" href="#tabmenu4">سایز 2</a></li>'
     '<li><a data-toggle="tab" href="#tabmenu5">سایز 3</a></li>'
     '<li><a data-toggle="tab" href="#tabmenu6">سایز 4</a></li>'
     '<li><a data-toggle="tab" href="#tabmenu7">سایز 5</a></li>'
     '<li><a data-toggle="tab" href="#tabmenu8">سایز 6</a></li></ul><hr>'),
        Div(
            Div(
                    'size1',
                    'price1',
            css_id="tabmenu3",css_class="tab-pane fade in active"),
            Div(
                        'size2',
                        'price2',
            css_id="tabmenu4", css_class="tab-pane fade"),
                Div(
                        'size3',
                        'price3',
            css_id="tabmenu5",css_class="tab-pane fade"),
            Div(
                        'size4',
                        'price4',
            css_id="tabmenu6", css_class="tab-pane fade"),
            Div(
                        'size5',
                        'price5',
            css_id="tabmenu7", css_class="tab-pane fade"),
            Div(
                        'size6',
                        'price6',
            css_id="tabmenu8", css_class="tab-pane fade")

        ,css_class="tab-content"),
            css_class="panel panel-warning"),
        Div(HTML('<div class=panel-heading><p class="panel-title">تصمیمات ادمین</p></div>'),

            Div('admin_confirmed','admin_featured'
                    ,css_class="panel-body")
                    ,css_class="panel panel-warning"),

        Div(
            HTML(
                '<button type="submit" class="btn btn-success">ثبت اطلاعات</button>'
                '<button type="button" class="btn btn-default" data-dismiss="modal">بستن فرم</button> '),

            css_class='modal-footer'),

        )
    class Meta:
        model = Carpet
        fields = ('title',
                'description',
                'category',
                'brush_num',
                'density',
                'colors_num',
                'yarn_type',
                'wool_yarn_type',
                'yarn_height',
                'antipilling',
                'image1', 'image2','image3', 'image4', 'image5','image6',
                'weight',
                'guarantee_len',
                'delivery_time',
                'color',
                'size1',
                'price1',
                'size2',
                'price2',
                'size3',
                'price3',
                'size4',
                'price4',
                'size5',
                'price5',
                'size6',
                'price6',
                'admin_featured',
                'admin_confirmed',)

        labels = {
            'brush_num': _("تعداد شانه"),
                'density': _("تراکم"),
                'colors_num': _("تعداد رنگ"),
                'yarn_type': _("نوع نخ"),
                'wool_yarn_type': _("نوع نخ پود"),
                'yarn_height': _("ارتفاع خواب"),
                'antipilling': _("ضد پرزدهی"),
                'weight': _("وزن"),
                'guarantee_len': _("زمان گارانتی"),
                'delivery_time': _("زمان تحویل"),
                'title': _("عنوان"),
                'category': _("دسته بندی"),
                'description': _("توصیف"),
                'image1': _("عکس اول"),
                'image2': _("عکس دوم"),
                'image3': _("عکس سوم"),
                'image4': _("عکس چهارم"),
                'image5': _("عکس پنجم"),
                'image6': _("عکس ششم"),
                'size1': _("اندازه"),
                'size2': _("اندازه"),
                'size3': _("اندازه"),
                'size4': _("اندازه"),
                'size5': _("اندازه"),
                'size6': _("اندازه"),
                'price1': _("قیمت(تومان)"),
                'price2': _("قیمت(تومان)"),
                'price3': _("قیمت(تومان)"),
                'price4': _("قیمت(تومان)"),
                'price5': _("قیمت(تومان)"),
                'price6': _("قیمت(تومان)"),
                'admin_confirmed': _("تایید ادمین"),
                'admin_featured': _("ویژه ادمین"),
        }


class CarpetCategoryForm(forms.ModelForm):
    helper = FormHelper()
    helper.form_id = "carpetcategoryform"
    helper.form_method = "post"
    helper.form_class = "js-upload-carpet-category-form"
    helper.layout = Layout(
        Div(HTML('<div class=panel-heading><p class="panel-title">اطلاعات دسته</p></div>'),
            Div('category_parent',
                'title',
                'image1',
                'image2',
                css_class="panel-body")
            , css_class="panel panel-warning"),
        Div(
            HTML(
                '<button type="submit" class="btn btn-success">ثبت اطلاعات</button>'
                '<button type="button" class="btn btn-default" data-dismiss="modal">بستن فرم</button> '),

            css_class='modal-footer'),

        )
    class Meta:
        model = CarpetCategory
        fields = ( 'category_parent',
                'title',
                'image1',
                'image2'
              )

        labels = {
            'category_parent': _("دسته اصلی"),
            'title': _("عنوان دسته"),
            'image1': _("تصویر 1 دسته"),
            'image2': _("تصویر 2 دسته"),
        }


class RegistrationForm(forms.ModelForm):
    username = forms.CharField(label="نام کاربری",widget=forms.TextInput())
    full_name = forms.CharField(required=False,label="نام کامل",widget=forms.TextInput())
    address = forms.CharField(required=False,label="آدرس",widget=forms.TextInput())
    email = forms.EmailField(label="ایمیل",widget=forms.EmailInput())
    password = forms.CharField(label="پسورد",max_length=50,min_length=6,
                                widget=forms.PasswordInput())
    helper = FormHelper()
    helper.form_method = "post"
    helper.layout = Layout(
        Div(HTML('<div class=panel-heading><p class="panel-title">اطلاعات ضروری</p></div>'),
            Div('username', 'password', 'email'
        ,css_class="panel-body")
        ,css_class="panel panel-warning"),
        Div(HTML('<div class=panel-heading><p class="panel-title">سایر اطلاعات</p></div>'),
            Div('full_name', 'address','phonenumber','zip_code'
        ,css_class="panel-body")
        ,css_class="panel panel-warning"),
        Div(
            HTML(
                '<button type="submit" class="btn btn-success">ثبت اطلاعات</button>'
                '<button type="button" class="btn btn-default" data-dismiss="modal">بستن فرم</button> '),

            css_class='modal-footer'),

        )
    class Meta:
        model = Profile
        fields = ('full_name', 'address', 'phonenumber', 'zip_code')
        labels = {
             'phonenumber':_('شماره تلفن'),
             'full_name':_('نام کامل'),
             'address':_('آدرس'),
             'zip_code':_('کد پستی'),
        }
    def clean(self):
        password1 = self.cleaned_data.get('password')
        users=User.objects.all()
        for un in users:
            if un.username == self.cleaned_data.get('username'):
                self._errors['username'] = ErrorList([u"نام کاربری قبلا ثبت شده است."])
        for un in users:
            if un.email == self.cleaned_data.get('email'):
                self._errors['email'] = ErrorList([u"ایمیل قبلا ثبت شده است."])

        return self.cleaned_data

    #Override of save method for saving both User and Profile objects
    def save(self, datas):
        u = User.objects.create_user(datas['username'],
                                 datas['email'],
                                 datas['password'])
        # u.is_active = False
        u.save()

        profile=Profile.objects.get(user=u)
        profile.user.username=datas['username']
        profile.user.email=datas['email']
        profile.user.set_password(datas['password'])
        profile.full_name=datas['full_name']
        profile.address=datas['address']
        profile.phonenumber=datas['phonenumber']
        profile.save()
        return u


class ComplaintAnswerQuickForm(forms.ModelForm):
    helper = FormHelper()
    helper.form_id = "complaintanswerquickform"
    helper.form_method = "post"

    helper.form_class = "js-upload-complaint-answer-form"
    helper.layout = Layout(
        Div(HTML('<div class=panel-heading><p class="panel-title">موضوع ایمیل</p></div>'),
            Div('subject',
            css_class="panel-body")
        ,css_class="panel panel-warning"),
        Div(HTML('<div class=panel-heading><p class="panel-title">متن ایمیل</p></div>'),
            Div(
            'text',
                css_class="panel-body")
            , css_class="panel panel-warning"),
        Div(
            HTML(
                '<button type="submit" class="btn btn-success">ثبت اطلاعات</button>'
                '<button type="button" class="btn btn-default" data-dismiss="modal">بستن فرم</button> '),

            css_class='modal-footer'),

        )
    class Meta:
        model = ComplaintAnswer
        fields = (
            'subject',
            'text',
                  )
        labels = {
            "subject": _("موضوع ایمیل"),
            "text": _("متن ایمیل"),
        }

    def __init__(self, complaint_pk, *args, **kwargs):
        self.compalint_pk = complaint_pk
        super(ComplaintAnswerQuickForm, self).__init__(*args, **kwargs)


class ContactAnswerQuickForm(forms.ModelForm):
    helper = FormHelper()
    helper.form_id = "contactanswerquickform"
    helper.form_method = "post"

    helper.form_class = "js-upload-contact-answer-form"
    helper.layout = Layout(
        Div(HTML('<div class=panel-heading><p class="panel-title">موضوع ایمیل</p></div>'),
            Div('subject',
            css_class="panel-body")
        ,css_class="panel panel-warning"),
        Div(HTML('<div class=panel-heading><p class="panel-title">متن ایمیل</p></div>'),
            Div(
            'text',
                css_class="panel-body")
            , css_class="panel panel-warning"),
        Div(
            HTML(
                '<button type="submit" class="btn btn-success">ثبت اطلاعات</button>'
                '<button type="button" class="btn btn-default" data-dismiss="modal">بستن فرم</button> '),

            css_class='modal-footer'),

        )
    class Meta:
        model = ContactAnswer
        fields = (
            'subject',
            'text',
                  )
        labels = {
            "subject": _("موضوع ایمیل"),
            "text": _("متن ایمیل"),
        }

    def __init__(self, contact_pk, *args, **kwargs):
        self.contact_pk = contact_pk
        super(ContactAnswerQuickForm, self).__init__(*args, **kwargs)


class PostForm(forms.ModelForm):
    helper = FormHelper()
    helper.form_id = "postform"
    helper.form_method = "post"
    helper.form_class = "js-upload-post-form"
    helper.layout = Layout(
        Div(HTML('<div class=panel-heading><p class="panel-title">داده های اولیه</p></div>'),
            Div('title',
                'category',
                'content',
            css_class="panel-body")
        ,css_class="panel panel-warning"),
        Div(HTML('<div class=panel-heading><p class="panel-title">تصاویر</p></div>'),
            Div(
        'image1',
        'image2',
        'image3',
        'image4',
        css_class="panel-body")
        ,css_class="panel panel-warning"),
        Div(HTML('<div class=panel-heading><p class="panel-title">تائیدیه مدیر</p></div>'),
            Div(
            'admin_confirmed',
                css_class="panel-body")
            , css_class="panel panel-warning"),
        Div(
            HTML(
                '<button type="submit" class="btn btn-success">ثبت اطلاعات</button>'
                '<button type="button" class="btn btn-default" data-dismiss="modal">بستن فرم</button> '),

            css_class='modal-footer'),

        )
    class Meta:
        model = Post
        fields = ( 'title',
            'category',
            'content',
            'admin_confirmed',
            'image1',
            'image2',
            'image3',
            'image4',
                  )

        labels = {
            "title": _("عنوان"),
            "category": _("دسته بندی"),
            "content": _("متن"),
            "admin_confirmed": _("تاییدیه"),
            "image1": _("عکس 1"),
            "image2": _("عکس 2"),
            "image3": _("عکس 3"),
            "image4": _("عکس 4"),
        }
