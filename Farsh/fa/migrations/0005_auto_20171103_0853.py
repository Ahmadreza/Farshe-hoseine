# -*- coding: utf-8 -*-
# Generated by Django 1.10.5 on 2017-11-03 13:53
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('fa', '0004_remove_post_post_status'),
    ]

    operations = [
        migrations.AlterField(
            model_name='profile',
            name='profile_account_num',
            field=models.CharField(blank=True, max_length=16, null=True),
        ),
        migrations.AlterField(
            model_name='profile',
            name='profile_bank',
            field=models.CharField(blank=True, max_length=20, null=True),
        ),
        migrations.AlterField(
            model_name='profile',
            name='profile_bank_branch',
            field=models.CharField(blank=True, max_length=50, null=True),
        ),
        migrations.AlterField(
            model_name='profile',
            name='profile_shaba',
            field=models.CharField(blank=True, max_length=24, null=True),
        ),
    ]
