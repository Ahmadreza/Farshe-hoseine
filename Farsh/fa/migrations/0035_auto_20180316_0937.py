# -*- coding: utf-8 -*-
# Generated by Django 1.11.7 on 2018-03-16 14:37
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('fa', '0034_product_product_admin_confirmed'),
    ]

    operations = [
        migrations.AddField(
            model_name='product',
            name='product_size',
            field=models.CharField(blank=True, max_length=10),
        ),
        migrations.AlterField(
            model_name='product',
            name='product_title',
            field=models.CharField(max_length=50),
        ),
    ]
