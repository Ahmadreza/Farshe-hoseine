# -*- coding: utf-8 -*-
# Generated by Django 1.11.7 on 2018-03-30 16:31
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('fa', '0045_auto_20180330_0950'),
    ]

    operations = [
        migrations.AddField(
            model_name='product',
            name='product_numbers1',
            field=models.SmallIntegerField(null=True),
        ),
        migrations.AddField(
            model_name='product',
            name='product_numbers2',
            field=models.SmallIntegerField(null=True),
        ),
        migrations.AddField(
            model_name='product',
            name='product_numbers3',
            field=models.SmallIntegerField(null=True),
        ),
        migrations.AddField(
            model_name='product',
            name='product_numbers4',
            field=models.SmallIntegerField(null=True),
        ),
        migrations.AddField(
            model_name='product',
            name='product_numbers5',
            field=models.SmallIntegerField(null=True),
        ),
    ]
