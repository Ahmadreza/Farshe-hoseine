# -*- coding: utf-8 -*-
# Generated by Django 1.11.7 on 2017-11-08 13:22
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('fa', '0008_auto_20171108_0524'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='carpricetemp',
            name='car',
        ),
        migrations.RemoveField(
            model_name='rentalrenttemp',
            name='rent',
        ),
        migrations.RemoveField(
            model_name='roompricetemp',
            name='room',
        ),
        migrations.AddField(
            model_name='shoppingbag',
            name='bag_admin_created',
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name='shoppingbag',
            name='bag_cancellation_penalty_percent',
            field=models.SmallIntegerField(default=0),
        ),
        migrations.AddField(
            model_name='shoppingbag',
            name='bag_cancelled',
            field=models.BooleanField(default=False),
        ),
        migrations.AlterField(
            model_name='ordereditem',
            name='ordered_car',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='fa.CarPrice'),
        ),
        migrations.DeleteModel(
            name='CarPriceTemp',
        ),
        migrations.DeleteModel(
            name='RentalRentTemp',
        ),
        migrations.DeleteModel(
            name='RoomPriceTemp',
        ),
    ]
