#!/usr/bin/env python
# -*- coding: utf-8 -*-from __future__ import unicode_literals


from django.template.loader import render_to_string
from django.contrib.auth.decorators import login_required
from django.http import JsonResponse,HttpResponse
from simple_rest import Resource
from fa.models import *
from app.models import *
from .forms import *
from django.shortcuts import render, get_object_or_404
import json
from django.db import transaction
from django.contrib.auth.models import User
from django.contrib.auth.decorators import user_passes_test
import itertools
import operator
from django.core.mail import send_mail, BadHeaderError, EmailMultiAlternatives


def most_common(L):
    SL = sorted((x, i) for i, x in enumerate(L))
    groups = itertools.groupby(SL, key=operator.itemgetter(0))

    def _auxfun(g):
        item, iterable = g
        count = 0
        min_index = len(L)
        for _, where in iterable:
            count += 1
            min_index = min(min_index, where)
        return count, -min_index

    return max(groups, key=_auxfun)[0]


def send_email(subject, body, email):
    print("EMAILLLL:",email)
    msg = EmailMultiAlternatives(subject, body, 'a.pourghodrat91@gmail.com', [email,])
    print("1111111111111")
    msg.attach_alternative(body, "text/html")
    print("22222222222")
    msg.send()
    print("333333333333")


class Carpets(Resource):
    def get(self,request):
        title=request.GET.get('Title')
        category=request.GET.get('Category')
        category_parent=request.GET.get('CategoryParent')
        carpets = Carpet.objects.all().filter(title__contains=title,category__category_parent__title__contains=category_parent,category__title__contains=category)
        totalAmountOfItems = carpets.count()
        pagesize = int(request.GET.get('pageSize'))
        pageindex = int(request.GET.get('pageIndex'))
        startindex = (pageindex - 1) * (pagesize)
        if (request.GET.get('sortField') and request.GET.get('sortOrder')):
            sortfield = request.GET.get('sortField')
            sortorder = request.GET.get('sortOrder')
            if (sortfield == "Title"):
                sort_str = "title"
            elif (sortfield == "CategoryParent"):
                sort_str = "category__category_parent__title"
            elif (sortfield == "Confirmed"):
                sort_str = "admin_confirmed"
            elif (sortfield == "Category"):
                sort_str = "category"
            elif (sortfield == "Description"):
                sort_str = "description"
            elif (sortfield == "Featured"):
                sort_str = "admin_featured"
            if (sortorder != 'asc'):
                sort_str = "-%s" % (sort_str)
            carpets = carpets.order_by(sort_str)
        carpets = carpets[startindex:(startindex + pagesize)]
        dictionaries = [obj.as_dict() for obj in carpets]
        return HttpResponse(json.dumps({"data": dictionaries, "itemsCount": totalAmountOfItems}, sort_keys=True),
                                content_type='application/json', status=200)

    def put(self, request, carpet_id):
        carpet = Carpet.objects.get(pk = carpet_id)
        carpet.admin_confirmed= True if request.PUT.get("Confirmed") == 'true' or request.PUT.get("Confirmed") == 'True' else False
        carpet.admin_featured= True if request.PUT.get("Featured") == 'true' or request.PUT.get("Featured") == 'True' else False
        carpet.save()
        return HttpResponse(status = 200)

    def delete(self, request, carpet_id):
        # all_bags=ShoppingBag.objects.filter(draft=False)
        carpet = Carpet.objects.get(pk=carpet_id)
        carpet.delete()
        return HttpResponse(status=200)


class Contacts(Resource):
    def get(self,request):
        full_name = request.GET.get('Name')
        email = request.GET.get('Email')
        phonenumber = request.GET.get('Phonenumber')
        contacts = Contact.objects.all().filter(full_name__contains=full_name, email__contains=email, phonenumber__contains=phonenumber)
        totalAmountOfItems = contacts.count()
        pagesize = int(request.GET.get('pageSize'))
        pageindex = int(request.GET.get('pageIndex'))
        startindex = (pageindex - 1) * (pagesize)
        if (request.GET.get('sortField') and request.GET.get('sortOrder')):
            sortfield = request.GET.get('sortField')
            sortorder = request.GET.get('sortOrder')
            if (sortfield == "Name"):
                sort_str = "full_name"
            elif (sortfield == "Email"):
                sort_str = "email"
            elif (sortfield == "Text"):
                sort_str = "text"
            elif (sortfield == "Answered"):
                sort_str = "answered"
            if (sortorder != 'asc'):
                sort_str = "-%s" % (sort_str)
            contacts = contacts.order_by(sort_str)
        contacts = contacts[startindex:(startindex + pagesize)]
        dictionaries = [obj.as_dict() for obj in contacts]
        return HttpResponse(json.dumps({"data": dictionaries, "itemsCount": totalAmountOfItems}, sort_keys=True),
                                content_type='application/json', status=200)

    def put(self, request, contact_id):
        contact = Contact.objects.get(pk = contact_id)
        contact.answered = True if request.PUT.get("Answered") == 'true' or request.PUT.get("Answered") == 'True' else False
        contact.save()
        return HttpResponse(status = 200)

    def delete(self, request, contact_id):
        contact = Contact.objects.get(pk=contact_id)
        contact.delete()
        return HttpResponse(status=200)


class Complaints(Resource):
    def get(self,request):
        full_name = request.GET.get('Name')
        email = request.GET.get('Email')
        phonenumber = request.GET.get('Phonenumber')
        complaints = Complaint.objects.all().filter(full_name__contains=full_name, email__contains=email, phonenumber__contains=phonenumber)
        totalAmountOfItems = complaints.count()
        pagesize = int(request.GET.get('pageSize'))
        pageindex = int(request.GET.get('pageIndex'))
        startindex = (pageindex - 1) * (pagesize)
        if (request.GET.get('sortField') and request.GET.get('sortOrder')):
            sortfield = request.GET.get('sortField')
            sortorder = request.GET.get('sortOrder')
            if (sortfield == "Name"):
                sort_str = "full_name"
            elif (sortfield == "Email"):
                sort_str = "email"
            elif (sortfield == "Text"):
                sort_str = "text"
            elif (sortfield == "Answered"):
                sort_str = "answered"
            if (sortorder != 'asc'):
                sort_str = "-%s" % (sort_str)
            complaints = complaints.order_by(sort_str)
        complaints = complaints[startindex:(startindex + pagesize)]
        dictionaries = [obj.as_dict() for obj in complaints]
        return HttpResponse(json.dumps({"data": dictionaries, "itemsCount": totalAmountOfItems}, sort_keys=True),
                                content_type='application/json', status=200)

    def put(self, request, complaint_id):
        complaint = Complaint.objects.get(pk = complaint_id)
        complaint.answered = True if request.PUT.get("Answered") == 'true' or request.PUT.get("Answered") == 'True' else False
        complaint.save()
        return HttpResponse(status = 200)

    def delete(self, request, complaint_id):
        complaint = Complaint.objects.get(pk=complaint_id)
        complaint.delete()
        return HttpResponse(status=200)


class CarpetCategories(Resource):
    def get(self,request):
        title=request.GET.get('Title')
        category_parent=request.GET.get('Parent')
        carpet_categories = CarpetCategory.objects.all().filter(title__contains=title,category_parent__title__contains=category_parent)
        totalAmountOfItems = carpet_categories.count()
        pagesize = int(request.GET.get('pageSize'))
        pageindex = int(request.GET.get('pageIndex'))
        startindex = (pageindex - 1) * (pagesize)
        if (request.GET.get('sortField') and request.GET.get('sortOrder')):
            sortfield = request.GET.get('sortField')
            sortorder = request.GET.get('sortOrder')
            if (sortfield == "Title"):
                sort_str = "title"
            elif (sortfield == "Parent"):
                sort_str = "category_parent__title"
            if (sortorder != 'asc'):
                sort_str = "-%s" % (sort_str)
            carpet_categories = carpet_categories.order_by(sort_str)
        carpet_categories = carpet_categories[startindex:(startindex + pagesize)]
        dictionaries = [obj.as_dict() for obj in carpet_categories]
        return HttpResponse(json.dumps({"data": dictionaries, "itemsCount": totalAmountOfItems}, sort_keys=True),
                                content_type='application/json', status=200)

    def put(self, request, category_id):
        carpet_category = CarpetCategory.objects.get(pk = category_id)
        carpet_category.save()
        return HttpResponse(status = 200)

    def delete(self, request, category_id):
        carpet_category = CarpetCategory.objects.get(pk=category_id)
        carpet_category.delete()
        return HttpResponse(status=200)


class Users(Resource):
    def get(self,request):
        username=request.GET.get('Username')
        full_name=request.GET.get('Name')
        address=request.GET.get('Address')
        zip_code=request.GET.get('ZipCode')
        phonenumber=request.GET.get('PhoneNumber')
        users = Profile.objects.all().filter(user__username__contains=username).filter(address__contains=address).filter(full_name__contains=full_name)
        totalAmountOfItems = users.count()
        pagesize = int(request.GET.get('pageSize'))
        pageindex = int(request.GET.get('pageIndex'))
        startindex = (pageindex - 1) * (pagesize)
        if (request.GET.get('sortField') and request.GET.get('sortOrder')):
            sortfield = request.GET.get('sortField')
            sortorder = request.GET.get('sortOrder')
            if (sortfield == "Username"):
                sort_str = "user__username"
            elif (sortfield == "Name"):
                sort_str = "full_name"
            elif (sortfield == "Address"):
                sort_str = "address"
            elif (sortfield == "PhoneNumber"):
                sort_str = "phonenumber"
            elif (sortfield == "ZipCode"):
                sort_str = "zip_code"
            if (sortorder != 'asc'):
                sort_str = "-%s" % (sort_str)
            users = users.order_by(sort_str)
        users = users[startindex:(startindex + pagesize)]
        dictionaries = [obj.as_dict() for obj in users]
        return HttpResponse(json.dumps({"data": dictionaries, "itemsCount": totalAmountOfItems}, sort_keys=True),
                                content_type='application/json', status=200)

    # def put(self, request, tour_id):
    #     tour = Tour.objects.get(pk = tour_id)
    #     tour.tour_admin_confirmed= True if request.PUT.get("Confirmed") == 'true' or request.PUT.get("Confirmed") == 'True' else False
    #     tour.tour_public_viewable = True if request.PUT.get("Viewable") == 'true' or request.PUT.get("Viewable") == 'True' else False
    #     tour.save()
    #     return HttpResponse(status = 200)

    def delete(self, request, user_id):
        profile = Profile.objects.get(pk=user_id)
        user=User.objects.get(username=profile.user.username)
        user.delete()
        return HttpResponse(status=200)


@user_passes_test(lambda u: u.is_superuser)
@login_required(login_url='/account/login/')
def administration(request):
        posts_num = 0
        carpets = Carpet.objects.all()
        carpets_num = carpets.count()
        confirmed_carpets_num = carpets.filter(admin_confirmed=True).count()
        tableaus = TableauRug.objects.all()
        tableaus_num = tableaus.count()
        confirmed_tableaus_num = tableaus.filter(admin_confirmed=True).count()
        products_num = carpets_num + tableaus_num
        confirmed_products_num = confirmed_carpets_num + confirmed_tableaus_num
        users_num = User.objects.all().count()
        superusers_num = User.objects.filter(is_superuser=True).count()
        bags = ShoppingBag.objects.filter(current_step=1)
        items_num = 0
        carpet_items_num = 0
        tableau_items_num = 0
        items_list = []
        carpet_items_list = []
        tableau_items_list = []
        max_items=None
        max_carpets=None
        max_tableaus=None
        for bag in bags:
            items = bag.items.all()
            items_num += items.count()
            for item in items:
                items_list.append(item)
                if item.item_carpet:
                    carpet_items_num += 1
                    carpet_items_list.append(item)
                else:
                    tableau_items_list.append(item)
                    tableau_items_num += 1

        if items_list == []:
            items_most_sell = None
            carpets_most_sell = None
            tableaus_most_sell = None
        else:
            items_most_sell = most_common(items_list)
            if items_most_sell.item_tableau:
                items_most_sell = items_most_sell.item_tableau
            else:
                items_most_sell = items_most_sell.item_carpet
            if carpet_items_list == []:
                carpets_most_sell = None
            else:
                carpets_most_sell = most_common(carpet_items_list)
                carpets_most_sell = carpets_most_sell.item_carpet
            if tableau_items_list == []:
                tableaus_most_sell = None
            else:
                tableaus_most_sell = most_common(tableau_items_list)
                tableaus_most_sell = tableaus_most_sell.item_tableau

        return render(request, "fa-administrator/index.html",{'posts_num':posts_num,'carpets_num':carpets_num,
                                                              'tableaus_num':tableaus_num,'users_num':users_num,
                                                              'superusers_num':superusers_num,'products_num':products_num,
                                                              'confirmed_carpets_num':confirmed_carpets_num,'confirmed_tableaus_num':confirmed_tableaus_num,
                                                              'confirmed_products_num':confirmed_products_num,'car_prices_num':None,
                                                              'items_num':items_num,'carpet_items_num':carpet_items_num,
                                                              'tableau_items_num':tableau_items_num,
                                                              'items_most_sell':items_most_sell,'carpets_most_sell':carpets_most_sell,
                                                              'tableaus_most_sell':tableaus_most_sell,'commited_num_out':None,
                                                              'all_users_num':None,'partners_num':None})


@user_passes_test(lambda u: u.is_superuser)
@login_required(login_url='/account/login/')
def admin_save(request,form, template_name):
    data = dict()
    if request.method == 'POST':
        if form.is_valid():
            # if template_name=='fa-administrator/fa-rentals/includes/partial_rental_create.html':
            #     if request.user.groups.filter(name="partner").exists():
            #         r=form.save(commit=False)
            #         r.rental_owner=request.user
            #         r.save()
            #     else:
            #         form.save()
            # elif template_name=='fa-administrator/fa-messages/includes/partial_message_create.html':
            #     # if request.user.groups.filter(name="partner").exists():
            #     m=form.save(commit=False)
            #     m.message_sender=request.user
            #     m.message_reply_number='---'
            #     m.message_status=2
            #     m.message_priority=1
            #     m.message_creation_date = jdatetime.datetime.now().strftime('%Y-%m-%d %H:%M')
            #     m.save()
            #     # else:
            #     #     form.save()
            if template_name=='fa-administrator/fa-posts/includes/partial_post_create.html':
                p=form.save(commit=False)
                p.author=request.user
            #     if (not p.post_creation_date) or p.post_creation_date == '':
            #         p.post_creation_date=datetime.datetime.now()
            #         p.post_creation_date_fa=jdatetime.datetime.now().strftime('%Y-%m-%d %H:%M')
            #     p.post_edition_date=datetime.datetime.now()
            #     p.post_edition_date_fa=jdatetime.datetime.now().strftime('%Y-%m-%d %H:%M')
            #     if not p.post_keywords:
            #         p.post_keywords='----'
                p.save()
            #     try:
            #         translator= Translator(from_lang="fa",to_lang="en")
            #         p.post_en_content = translator.translate(SafeString(p.post_fa_content))
            #         p.post_en_title = translator.translate(p.post_fa_title)
            #         translator= Translator(from_lang="fa",to_lang="es")
            #         p.post_es_content = translator.translate(SafeString(p.post_fa_content))
            #         p.post_es_title = translator.translate(p.post_fa_title)
            #         # p.save()
            #         translator= Translator(from_lang="fa",to_lang="ar")
            #         p.post_ar_content = translator.translate(SafeString(p.post_fa_content))
            #         p.post_ar_title = translator.translate(p.post_fa_title)
            #         translator= Translator(from_lang="fa",to_lang="fr")
            #         p.post_fr_content = translator.translate(SafeString(p.post_fa_content))
            #         p.post_fr_title = translator.translate(p.post_fa_title)
            #         # p.save()
            #         translator= Translator(from_lang="fa",to_lang="zh")
            #         p.post_zh_content = translator.translate(SafeString(p.post_fa_content))
            #         p.post_zh_title = translator.translate(p.post_fa_title)
            #         p.save()
            #     except:
            #         print("translation error")
            form.save()
            data['form_is_valid'] = True

        else:
            data['form_is_valid'] = False
    context = {'form': form}
    data['html_form'] = render_to_string(template_name, context, request=request)
    return JsonResponse(data)


@user_passes_test(lambda u: u.is_superuser)
@login_required(login_url='/account/login/')
def admin_carpets_index(request):
    return render(request,'fa-administrator/fa-carpets/carpets-list.html')


@user_passes_test(lambda u: u.is_superuser)
@login_required(login_url='/account/login/')
def admin_complaints_index(request):
    return render(request,'fa-administrator/fa-complaints/complaints_list.html')


@user_passes_test(lambda u: u.is_superuser)
@login_required(login_url='/account/login/')
def admin_contacts_index(request):
    return render(request,'fa-administrator/fa-contacts/contacts_list.html')


@user_passes_test(lambda u: u.is_superuser)
@login_required(login_url='/account/login/')
def admin_carpet_create(request):
    if request.method == 'POST':
        # if request.user.groups.filter(name="partner").exists():
            form = CarpetForm(request.POST,request.FILES)
        # else:
        #     form = RentalAdminForm(request.POST,request.FILES)
        # isnew=True
    else:
        # if request.user.groups.filter(name="partner").exists():
            form = CarpetForm()
        # else:
        #     form=RentalAdminForm()
        # isnew=False
    return admin_save(request, form, 'fa-administrator/fa-carpets/includes/partial_carpet_create.html')


@user_passes_test(lambda u: u.is_superuser)
@login_required(login_url='/account/login/')
def admin_carpet_update(request,pk):
    carpet = get_object_or_404(Carpet, pk=pk)
    if request.method == 'POST':
        # if request.user.groups.filter(name="partner").exists():
            form = CarpetForm(request.POST, request.FILES,instance=carpet)
        # else:
        #     form = RentalAdminForm(request.POST, request.FILES,instance=category)
    else:
        # if request.user.groups.filter(name="partner").exists():
            form = CarpetForm(instance=carpet)
        # else:
        #     form = RentalAdminForm(instance=category)
    return admin_save(request, form, 'fa-administrator/fa-carpets/includes/partial_carpet_edit.html')


@user_passes_test(lambda u: u.is_superuser)
def get_carpets(request):
  if request.is_ajax():
    q = request.GET.get('term')
    if(q):
        if(len(q)>0):
            carpets = Carpet.objects.filter(title__icontains=q)
            results = []
            for carpet in carpets:
                carpet_json = {}
                carpet_json['id']=carpet.id
                carpet_json['text'] = carpet.title


            #place_json = pl.location_title + "," + pl.location_country
                results.append(carpet_json)
            data = json.dumps(results)
    else:

        data = 'fail'
    mimetype = 'application/json'
    return HttpResponse(data, mimetype)


@user_passes_test(lambda u: u.is_superuser)
def get_carpet_categories(request):
  if request.is_ajax():
    q = request.GET.get('term')
    if(q):
        if(len(q)>0):
            categories = CarpetCategory.objects.filter(title__icontains=q)
            results = []
            for category in categories:
                category_json = {}
                category_json['id']=category.id
                category_json['text'] = category.title


            #place_json = pl.location_title + "," + pl.location_country
                results.append(category_json)
            data = json.dumps(results)
    else:

        data = 'fail'
    mimetype = 'application/json'
    return HttpResponse(data, mimetype)


@user_passes_test(lambda u: u.is_superuser)
@login_required(login_url='/account/login/')
def admin_carpet_categories_index(request):
    return render(request,'fa-administrator/fa-carpet-categories/carpet-categories-list.html')


@user_passes_test(lambda u: u.is_superuser)
@login_required(login_url='/account/login/')
def admin_carpet_category_create(request):
    if request.method == 'POST':
        form = CarpetCategoryForm(request.POST,request.FILES)
    else:
        form = CarpetCategoryForm()
    return admin_save(request, form, 'fa-administrator/fa-carpet-categories/includes/partial_carpet_category_create.html')


@user_passes_test(lambda u: u.is_superuser)
@login_required(login_url='/account/login/')
def admin_carpet_category_update(request,pk):
    carpet_category = get_object_or_404(CarpetCategory, pk=pk)
    if request.method == 'POST':
        form = CarpetCategoryForm(request.POST, request.FILES,instance=carpet_category)
    else:
            form = CarpetCategoryForm(instance=carpet_category)
    return admin_save(request, form, 'fa-administrator/fa-carpet-categories/includes/partial_carpet_category_edit.html')


@user_passes_test(lambda u: u.is_superuser)
@login_required(login_url='/account/login/')
def admin_user_index(request):
    return render(request,'fa-administrator/fa-users/user_list.html')


@user_passes_test(lambda u: u.is_superuser)
@login_required(login_url='/account/login/')
def register_admin(request):
    datas=dict()
    temp=dict()
    registration_form = RegistrationForm()
    if request.method == 'POST':
        form = RegistrationForm(request.POST,request.FILES)
        if form.is_valid():
            datas['username']=form.cleaned_data['username']
            datas['email']=form.cleaned_data['email']
            datas['password']=form.cleaned_data['password']
            datas['full_name']=form.cleaned_data['full_name']
            datas['address']=form.cleaned_data['address']
            datas['phonenumber']=form.cleaned_data['phonenumber']
            datas['zip_code']=form.cleaned_data['zip_code']
            form.save(datas) #Save the user and his profile
            datas={}
            datas['form_is_valid'] = True
            request.session['registered']=True #For display purposes
        else:
            registration_form = form #Display form with error messages (incorrect fields, etc)
            datas['form_is_valid'] = False
    context = {'form': registration_form}
    datas['html_form'] = render_to_string('fa-administrator/fa-users/includes/partial_user_create.html', context, request=request)
    return JsonResponse(datas)


@user_passes_test(lambda u: u.is_superuser)
@login_required(login_url='/account/login/')
@transaction.atomic
def admin_user_update(request,pk):
    datas=dict()
    user = get_object_or_404(Profile, pk=pk)
    registration_form = RegistrationForm(instance=user)
    if request.method == 'POST':
        form = RegistrationForm(request.POST,request.FILES,instance=user)
        if form.is_valid():
            # datas['update']="Yes"
            # datas['pk']=pk
            datas['username']=form.cleaned_data['username']
            datas['email']=form.cleaned_data['email']
            datas['password']=form.cleaned_data['password']
            datas['full_name']=form.cleaned_data['full_name']
            datas['address']=form.cleaned_data['address']
            datas['phonenumber']=form.cleaned_data['phonenumber']
            form.save(datas) #Save the user and his profile
            datas={}
            datas['form_is_valid'] = True
            request.session['registered']=True #For display purposes
        else:
            registration_form = form #Display form with error messages (incorrect fields, etc)
            datas['form_is_valid'] = False

    context = {'form': registration_form}
    datas['html_form'] = render_to_string('fa-administrator/fa-users/includes/partial_user_edit.html', context, request=request)
    return JsonResponse(datas)


@user_passes_test(lambda u: u.is_superuser)
@login_required(login_url='/account/login/')
def fa_admin_complaint_answer_quick_create(request,pk):
    data=dict()
    complaint = get_object_or_404(Complaint, pk=pk)
    if request.method == 'POST':
        form = ComplaintAnswerQuickForm(complaint_pk=pk, data=request.POST)
        if form.is_valid():
            c=form.save(commit=False)
            c.complaint=complaint
            # date1=jdatetime.datetime.strptime(str(form.cleaned_data['rental_rent_start_date']), '%Y-%m-%d')
            # date2=jdatetime.datetime.strptime(str(form.cleaned_data['rental_rent_end_date']), '%Y-%m-%d')
            # r.rental_rent_start_date_en=date1.togregorian()
            # r.rental_rent_end_date_en=date2.togregorian()
            # if form.cleaned_data['rental_rent_discount_percent']:
            #     r.rental_rent_discount = int(form.cleaned_data['rental_rent_price']) * (1- int(form.cleaned_data['rental_rent_discount_percent'])/100)
            #     if form.cleaned_data['rental_rent_price_dollar'] :
            #         r.rental_rent_discount_dollar = float(form.cleaned_data['rental_rent_price_dollar']) * (1- int(form.cleaned_data['rental_rent_discount_percent'])/100)
            #     if form.cleaned_data['rental_rent_price_euro'] :
            #         r.rental_rent_discount_euro = float(form.cleaned_data['rental_rent_price_euro']) * (1- int(form.cleaned_data['rental_rent_discount_percent'])/100)
            #     if form.cleaned_data['rental_rent_price_pound'] :
            #         r.rental_rent_discount_pound = float(form.cleaned_data['rental_rent_price_pound']) * (1- int(form.cleaned_data['rental_rent_discount_percent'])/100)
            # r.rental_title=Rental.objects.get(pk=pk).rental_title
            c.complaint = complaint
            c.save()
            data['form_is_valid'] = True
            complaint.answered = True
            complaint.save()
            send_email(c.subject, c.text, c.complaint.email)
            print("DONEEEE")

        else:
            data['form_is_valid'] = False
    else:
        form = ComplaintAnswerQuickForm(complaint_pk=pk)
    context = {'form': form, 'pk':complaint.pk}
    data['html_form'] = render_to_string('fa-administrator/fa-complaints/includes/partial_complaint_answer_quick_create.html', context, request=request)
    return JsonResponse(data)

@user_passes_test(lambda u: u.is_superuser)
@login_required(login_url='/account/login/')
def text_complaint(request,pk):
    data = dict()
    complaint = Complaint.objects.get(pk=pk)
    complaint_text = complaint.text
    context = {'complaint_text': complaint_text}
    data['html_form'] = render_to_string('fa-administrator/fa-complaints/includes/text_complaint.html', context, request=request)
    return JsonResponse(data)


@user_passes_test(lambda u: u.is_superuser)
@login_required(login_url='/account/login/')
def fa_admin_contact_answer_quick_create(request,pk):
    data=dict()
    contact = get_object_or_404(Contact, pk=pk)
    if request.method == 'POST':
        form = ContactAnswerQuickForm(contact_pk=pk, data=request.POST)
        if form.is_valid():
            c=form.save(commit=False)
            c.contact=contact
            # date1=jdatetime.datetime.strptime(str(form.cleaned_data['rental_rent_start_date']), '%Y-%m-%d')
            # date2=jdatetime.datetime.strptime(str(form.cleaned_data['rental_rent_end_date']), '%Y-%m-%d')
            # r.rental_rent_start_date_en=date1.togregorian()
            # r.rental_rent_end_date_en=date2.togregorian()
            # if form.cleaned_data['rental_rent_discount_percent']:
            #     r.rental_rent_discount = int(form.cleaned_data['rental_rent_price']) * (1- int(form.cleaned_data['rental_rent_discount_percent'])/100)
            #     if form.cleaned_data['rental_rent_price_dollar'] :
            #         r.rental_rent_discount_dollar = float(form.cleaned_data['rental_rent_price_dollar']) * (1- int(form.cleaned_data['rental_rent_discount_percent'])/100)
            #     if form.cleaned_data['rental_rent_price_euro'] :
            #         r.rental_rent_discount_euro = float(form.cleaned_data['rental_rent_price_euro']) * (1- int(form.cleaned_data['rental_rent_discount_percent'])/100)
            #     if form.cleaned_data['rental_rent_price_pound'] :
            #         r.rental_rent_discount_pound = float(form.cleaned_data['rental_rent_price_pound']) * (1- int(form.cleaned_data['rental_rent_discount_percent'])/100)
            # r.rental_title=Rental.objects.get(pk=pk).rental_title
            c.contact = contact
            c.save()
            data['form_is_valid'] = True
            contact .answered = True
            contact .save()
            send_email(c.subject, c.text, c.contact.email)
            print("DONEEEE")

        else:
            data['form_is_valid'] = False
    else:
        form = ContactAnswerQuickForm(contact_pk=pk)
    context = {'form': form, 'pk':contact.pk}
    data['html_form'] = render_to_string('fa-administrator/fa-contacts/includes/partial_contact_answer_quick_create.html', context, request=request)
    return JsonResponse(data)


@user_passes_test(lambda u: u.is_superuser)
@login_required(login_url='/account/login/')
def text_contact(request,pk):
    data = dict()
    contact = Contact.objects.get(pk=pk)
    contact_text = contact.text
    context = {'contact_text': contact_text}
    data['html_form'] = render_to_string('fa-administrator/fa-contacts/includes/text_contact.html', context, request=request)
    return JsonResponse(data)


@user_passes_test(lambda u: u.is_superuser)
@login_required(login_url='/account/login/')
def fa_admin_post_index(request):
    return render(request,'fa-administrator/fa-posts/post-list.html')


@user_passes_test(lambda u: u.is_superuser)
@login_required(login_url='/account/login/')
def fa_admin_post_create(request):
    if request.method == 'POST':
        form = PostForm(request.POST,request.FILES)
        isnew=True
    else:
        form = PostForm()
        isnew=False
    return admin_save(request, form, 'fa-administrator/fa-posts/includes/partial_post_create.html')


class Posts(Resource):
    def get(self,request):
        title=request.GET.get('Title')
        category=request.GET.get('Category')
        creation_date=request.GET.get('Creation_date')
        author=request.GET.get('User')

        posts = Post.objects.all()
        print("Postsss:",posts)
        # posts = Post.objects.all()
        totalAmountOfItems = posts.count()
        pagesize = int(request.GET.get('pageSize'))
        pageindex = int(request.GET.get('pageIndex'))
        startindex = (pageindex - 1) * (pagesize)
        if (request.GET.get('sortField') and request.GET.get('sortOrder')):
            sortfield = request.GET.get('sortField')
            sortorder = request.GET.get('sortOrder')
            if (sortfield == "Title"):
                sort_str = "title"
            if (sortfield == "Category"):
                sort_str = "category"
            elif (sortfield == "Creation_date"):
                sort_str = "creation_date"
            elif (sortfield == "Confirmed"):
                sort_str = "admin_confirmed"
            elif (sortfield == "User"):
                sort_str = "author__username"
            if (sortorder != 'asc'):
                sort_str = "-%s" % (sort_str)
            posts = posts.order_by(sort_str)
        posts = posts[startindex:(startindex + pagesize)]
        dictionaries = [obj.as_dict() for obj in posts]
        return HttpResponse(json.dumps({"data": dictionaries, "itemsCount": totalAmountOfItems}, sort_keys=True),
                                content_type='application/json', status=200)

    def put(self, request, post_id):
        post = Post.objects.get(pk = post_id)
        post.admin_confirmed= True if request.PUT.get("Confirmed") == 'true' or request.PUT.get("Confirmed") == 'True' else False
        post.save()
        return HttpResponse(status = 200)

    def delete(self, request, post_id):
        post = Post.objects.get(pk=post_id)
        post.delete()
        return HttpResponse(status=200)


@user_passes_test(lambda u: u.is_superuser)
@login_required(login_url='/account/login/')
def fa_admin_post_update(request,pk):
    post = get_object_or_404(Post, pk=pk)
    if request.method == 'POST':
        form = PostForm(request.POST, request.FILES,instance=post)
    else:
        form = PostForm(instance=post)
    return admin_save(request, form, 'fa-administrator/fa-posts/includes/partial_post_edit.html')

