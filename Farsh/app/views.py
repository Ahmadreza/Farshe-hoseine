from django.contrib.auth import login, authenticate
from django.shortcuts import render, redirect
from .forms import *
from .models import *
from django.http import HttpResponseRedirect
from django.contrib.auth.decorators import login_required
# Create your views here.


def homepage(request):

    carpets_categories_list=[]
    carpets_categories_parent_list=[]
    carpets_categories =  CarpetCategory.objects.all()

    for c in carpets_categories:
        if c.category_parent == None:
            carpets_categories_parent_list.append(c)
    for i,c in enumerate(carpets_categories_parent_list):
        carpets_categories_list.append([c])
        for c_child in carpets_categories:
            if c_child.category_parent == c:
                carpets_categories_list[i].append(c_child)

    tableaus_categories_list=[]
    tableaus_categories_parent_list=[]
    tableaus_categories =  TableauRugCategory.objects.all()
    for c in tableaus_categories:
        if c.category_parent == None:
            tableaus_categories_parent_list.append(c)
    for i,c in enumerate(tableaus_categories_parent_list):
        tableaus_categories_list.append([c])
        for c_child in tableaus_categories:
            if c_child.category_parent == c:
                tableaus_categories_list[i].append(c_child)

    recent_products = []

    return render(request,'home_page.html',{'carpets_categories':carpets_categories_list,'tableaus_categories':tableaus_categories_list})


def products(request, query=None):

    carpets_categories_list=[]
    carpets_categories_parent_list=[]
    carpets_categories =  CarpetCategory.objects.all()

    for c in carpets_categories:
        if c.category_parent == None:
            carpets_categories_parent_list.append(c)
    for i,c in enumerate(carpets_categories_parent_list):
        carpets_categories_list.append([c])
        for c_child in carpets_categories:
            if c_child.category_parent == c:
                carpets_categories_list[i].append(c_child)
                if c.title == "700 شانه" :
                    if c_child.title == "تست ادمین":
                        print("HEREEEE")
                        print(c_child.category_parent)

    tableaus_categories_list=[]
    tableaus_categories_parent_list=[]
    tableaus_categories =  TableauRugCategory.objects.all()
    for c in tableaus_categories:
        if c.category_parent == None:
            tableaus_categories_parent_list.append(c)
    for i,c in enumerate(tableaus_categories_parent_list):
        tableaus_categories_list.append([c])
        for c_child in tableaus_categories:
            if c_child.category_parent == c:
                tableaus_categories_list[i].append(c_child)

    carpets = Carpet.objects.all()
    return render(request,'products.html',{'type':"direct", 'carpets_categories_parents':carpets_categories_parent_list, 'carpets_categories':carpets_categories_list, 'tableaus_categories':tableaus_categories_list, 'carpets':carpets})


def products1(request, query):

    carpets_categories_list=[]
    carpets_categories_parent_list=[]
    carpets_categories =  CarpetCategory.objects.all()

    for c in carpets_categories:
        if c.category_parent == None:
            carpets_categories_parent_list.append(c)
    for i,c in enumerate(carpets_categories_parent_list):
        carpets_categories_list.append([c])
        for c_child in carpets_categories:
            if c_child.category_parent == c:
                carpets_categories_list[i].append(c_child)
                if c.title == "700 شانه" :
                    if c_child.title == "تست ادمین":
                        print("HEREEEE")
                        print(c_child.category_parent)

    tableaus_categories_list=[]
    tableaus_categories_parent_list=[]
    tableaus_categories =  TableauRugCategory.objects.all()
    for c in tableaus_categories:
        if c.category_parent == None:
            tableaus_categories_parent_list.append(c)
    for i,c in enumerate(tableaus_categories_parent_list):
        tableaus_categories_list.append([c])
        for c_child in tableaus_categories:
            if c_child.category_parent == c:
                tableaus_categories_list[i].append(c_child)

    ccc=CarpetCategory.objects.get(pk=query)
    print(ccc.title)
    car=Carpet.objects.filter(category__category_parent__title__contains=ccc.title)

    return render(request,'products.html',{"type": "click",'tableaus_categories':tableaus_categories_list, 'carpets_categories_parents':carpets_categories_parent_list,'carpets_categories':carpets_categories_list,'tableaus_categories':tableaus_categories_list, })



def contact_us(request):

    if request.method == 'POST':
        form = ContactUsForm(request.POST)
        if form.is_valid():
            form.save()

    else:
        form = SignUpForm()

    return render(request, 'contact_us.html', {'form': form})

@login_required(login_url='/accounts/login/')
def sabad_kharid(request):

    return render(request,'sabad_kharid.html')


def complaint(request):

    if request.method == 'POST':
        form = ComplaintForm(request.POST)
        if form.is_valid():
            form.save()

    else:
        form = ComplaintForm()

    return render(request, 'complaint.html', {'form': form})


def signup(request):

    if request.method == 'POST':
        form = SignUpForm(request.POST)
        if form.is_valid():
            user = form.save()
            user.refresh_from_db()  # load the profile instance created by the signal
            user.profile.phonenumber = form.cleaned_data.get('phonenumber')
            user.profile.address = form.cleaned_data.get('address')
            user.profile.full_name = form.cleaned_data.get('full_name')
            user.save()
            raw_password = form.cleaned_data.get('password1')
            user = authenticate(username=user.username, password=raw_password)
            login(request, user)
            return redirect('home')
    else:
        form = SignUpForm()

    return render(request, 'signup.html', {'form': form})


def factor(request):

    return render(request,'factor.html')

def about_us(request):

    return render(request,'about_us.html')

def sabte_sefaresh(request, bag_id):

    return render(request,'sabte_sefaresh.html',{"bag_id":bag_id})


def peygiri_sefaresh(request):

    return render(request,'peygiri_sefaresh.html')

def in_blog(request,post_id):

    post = Post.objects.get(pk=post_id)
    return render(request,'in_blog.html',{"post":post})

def blog(request):

    posts = Post.objects.filter(admin_confirmed=True)
    return render(request,'blog.html', {'posts':posts})

def profile(request):

    return render(request,'profile.html')

def after_sabte_sefaresh(request,bag_id):

    return render(request,'after_sabte_sefaresh.html')

def help(request):

    return render(request,'help.html')
#
# def login(request):
#
#     return render(request,'login.html')

def not_found(request):

    return render(request,'404.html')
def search_result(request):

    return render(request,'search_result.html')


def product_page(request, product_id):

    product = Carpet.objects.get(pk=product_id)
    return render(request,'product_page.html',{'product':product})

@login_required(login_url='/accounts/login/')
def order(request):

    if request.method == 'POST':
        item = OrderedItem.objects.create(item_carpet = Carpet.objects.get(pk=int(request.POST.get('product',0))), numbers = request.POST.get('numbers',0),color = request.POST.get('color',0), price = request.POST.get('price',0),total_price = int(request.POST.get('numbers',0)) * int(request.POST.get('price',0)))
        bag = ShoppingBag.objects.get(owner=request.user, draft=True)
        bag.items.add(item)
        total_price = 0
        for item in bag.items.all():
            total_price += item.total_price
        bag.total_price = total_price
        bag.save()
        return HttpResponseRedirect('/product_page/carpet/'+request.POST.get('product',0))

@login_required(login_url='/accounts/login/')
def remove_item(request, item_id):

    item = OrderedItem.objects.get(pk=item_id)
    bag = ShoppingBag.objects.get(owner=request.user, draft=True)
    bag.total_price -= item.total_price
    item.delete()
    bag.save()
    return HttpResponseRedirect('/cart/')

@login_required(login_url='/accounts/login/')
def update_address(request,bag_id):

    if request.method == 'POST':
        user=request.user
        user.profile.address = request.POST.get('address',0)
        user.profile.zip_code = request.POST.get('zip_code',0)
        user.profile.phonenumber = request.POST.get('phonenumber',0)
        user.save()
        return HttpResponseRedirect('/order_registration/'+bag_id)
