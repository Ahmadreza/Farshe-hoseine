from .models import ShoppingBag,CarpetCategory

def get_bag(request):
    shopping_bag=items=''
    items_count=0
    if request.user.is_authenticated:
        if not ShoppingBag.objects.filter(owner=request.user,draft=True).exists():
            shopping_bag=ShoppingBag.objects.create(owner=request.user, draft=True, total_price=0)
        else:
            shopping_bag=ShoppingBag.objects.get(owner=request.user, draft=True)
        items=shopping_bag.items.all()
        items_count=items.count()
        return {'items':items,'items_count':items_count,'bag':shopping_bag}
    else:
        return {}

def get_categories(request):
    categories = CarpetCategory.objects.all()
    # print('***********',categories)
    return {'categories':categories}
