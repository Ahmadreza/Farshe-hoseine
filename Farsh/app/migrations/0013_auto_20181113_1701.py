# Generated by Django 2.1.3 on 2018-11-13 17:01

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0012_auto_20181113_1624'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='carpet',
            name='colors',
        ),
        migrations.AddField(
            model_name='carpet',
            name='color1',
            field=models.CharField(blank=True, choices=[('قرمز', 'قرمز'), ('آبی', 'آبی'), ('سرمه ای', 'سرمه ای'), ('کرمی', 'کرمی'), ('بنفش', 'بنفش')], max_length=10, null=True),
        ),
        migrations.AddField(
            model_name='carpet',
            name='color2',
            field=models.CharField(blank=True, choices=[('قرمز', 'قرمز'), ('آبی', 'آبی'), ('سرمه ای', 'سرمه ای'), ('کرمی', 'کرمی'), ('بنفش', 'بنفش')], max_length=10, null=True),
        ),
        migrations.AddField(
            model_name='carpet',
            name='color3',
            field=models.CharField(blank=True, choices=[('قرمز', 'قرمز'), ('آبی', 'آبی'), ('سرمه ای', 'سرمه ای'), ('کرمی', 'کرمی'), ('بنفش', 'بنفش')], max_length=10, null=True),
        ),
        migrations.AddField(
            model_name='carpet',
            name='color4',
            field=models.CharField(blank=True, choices=[('قرمز', 'قرمز'), ('آبی', 'آبی'), ('سرمه ای', 'سرمه ای'), ('کرمی', 'کرمی'), ('بنفش', 'بنفش')], max_length=10, null=True),
        ),
        migrations.AddField(
            model_name='carpet',
            name='color5',
            field=models.CharField(blank=True, choices=[('قرمز', 'قرمز'), ('آبی', 'آبی'), ('سرمه ای', 'سرمه ای'), ('کرمی', 'کرمی'), ('بنفش', 'بنفش')], max_length=10, null=True),
        ),
    ]
