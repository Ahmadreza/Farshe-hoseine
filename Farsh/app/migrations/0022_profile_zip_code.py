# Generated by Django 2.1.3 on 2018-11-24 21:42

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0021_auto_20181124_2122'),
    ]

    operations = [
        migrations.AddField(
            model_name='profile',
            name='zip_code',
            field=models.CharField(blank=True, max_length=10, null=True),
        ),
    ]
