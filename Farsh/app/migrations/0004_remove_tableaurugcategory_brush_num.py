# -*- coding: utf-8 -*-
# Generated by Django 1.11.7 on 2018-11-04 17:36
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0003_auto_20181104_1636'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='tableaurugcategory',
            name='brush_num',
        ),
    ]
