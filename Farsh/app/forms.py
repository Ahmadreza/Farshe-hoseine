from django import forms
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from .models import Complaint,Contact, OrderedItem


class SignUpForm(UserCreationForm):

    phonenumber = forms.CharField(help_text='09xxxxxxxxx')
    address = forms.CharField(help_text='آدرس محل ارسال سفارش را وارد نمایید.')
    full_name = forms.CharField(help_text='لطفا نام کامل خود را وارد نمایید.')

    class Meta:
        model = User
        fields = ('full_name', 'phonenumber', 'email', 'address', 'username', 'password1', 'password2',  )


class ComplaintForm(forms.ModelForm):

    class Meta:
        model = Complaint
        fields = ('full_name', 'phonenumber', 'email', 'text')


class ContactUsForm(forms.ModelForm):

    class Meta:
        model = Contact
        fields = ('full_name', 'phonenumber', 'email', 'text')
