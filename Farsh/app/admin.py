from django.contrib import admin
from .models import *
# Register your models here.
admin.site.register(CarpetCategory)
admin.site.register(Carpet)
admin.site.register(TableauRugCategory)
admin.site.register(TableauRug)
admin.site.register(OrderedItem)
admin.site.register(ShoppingBag)
admin.site.register(Profile)
admin.site.register(Complaint)
admin.site.register(Contact)
admin.site.register(ComplaintAnswer)
admin.site.register(ContactAnswer)
admin.site.register(Post)
