from django.db import models
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.dispatch import receiver
from multiselectfield import MultiSelectField

#todo: think for deletes

CARPET_SIZES = (("6 متری", "6 متری"), ("8 متری", "8 متری"), ("12 متری", "12 متری"), ("18 متری", "18 متری"), ("درخواستی", "درخواستی"))
TABLEAURUG_SIZE = (("100*150", "100*150"), ("70*100", "70*100"), ("50*100", "50*100"), ("50*70", "50*70"), ("درخواستی", "درخواستی"))
CARPET_COLORS = (("قرمز", "قرمز"), ("آبی", "آبی"), ("سرمه ای", "سرمه ای"), ("کرمی", "کرمی"), ("بنفش", "بنفش"), ("وارد-نشده", "وارد-نشده"))
BAG_STEPS = (("0", "پیش ثبت"), ("1", "ثبت سفارش"), ("2", "مرحله بافت"),("3", "مرحله تکمیل"), ("4", "تحویل به باربری" ), ("5", "تحویل به مشتری"))
POST_CATEGORY=(("خبری","خبری"),("علمی","علمی"),("صنعت","صنعت"),('دسته_بندی_نشده','دسته بندی نشده'))
# Create your models here.


class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    full_name = models.CharField(max_length = 50, null=True, blank=True)
    phonenumber = models.CharField(max_length=15, null=True, blank=True)
    address = models.TextField(max_length=500, blank=True, default='----------')
    zip_code = models.CharField(max_length=10, blank=True, default='----------')

    def __str__(self):
        return "%s" % self.user.username

    def as_dict(self):
        return {
            "id": self.id,
            "Username": self.user.username,
            "Name": self.full_name,
            "Address": self.address,
            "ZipCode": self.zip_code,
            "PhoneNumber": self.phonenumber,
        }


@receiver(post_save, sender=User)
def update_user_profile(sender, instance, created, **kwargs):
    if created:
        Profile.objects.create(user=instance)
    instance.profile.save()


class CarpetCategory(models.Model):
    title = models.CharField(max_length=30, default="وارد نشده")
    category_parent = models.ForeignKey('self', blank=True, null=True, on_delete=models.PROTECT)
    image1 = models.ImageField(null=True, blank=True, upload_to='carpet-category/images/')
    image2 = models.ImageField(null=True, blank=True, upload_to='carpet-category/images/')

    def __str__(self):
        if self.category_parent:
            return "عنوان: %s / %s" % self.title,self.category_parent.title
        else:
            return "%s" % self.title

    def as_dict(self):
        # image1=""
        # image2=""
        # if self.image1:
        #     image1=self.image1.url
        # else:
        #     image1="بدون تصویر"
        # if self.image2:
        #     image2=self.image2.url
        # else:
        #     image2="بدون تصویر"

        if self.category_parent:
            p = self.category_parent.title
        else:
            p = 'ندارد'
        return {
            "id": self.id,
            "Title": self.title,
            "Parent": p,
            # "Image1": str(image1),
            # "Image2": str(image2)
        }


class Carpet(models.Model):
    title = models.CharField(max_length=30, default='وارد نشده')
    description = models.TextField(null=True, blank=True)
    category = models.ForeignKey(CarpetCategory, blank=True, null=True, on_delete=models.PROTECT)
    brush_num = models.PositiveIntegerField(default=0)
    density = models.PositiveIntegerField(default=0)
    colors_num = models.PositiveIntegerField(default=0)
    yarn_type = models.CharField(max_length=25, default='وارد نشده')
    wool_yarn_type = models.CharField(max_length=15, default='وارد نشده')
    yarn_height = models.FloatField(null=True, blank=True)
    antipilling = models.BooleanField(default=False)
    weight = models.PositiveIntegerField(default=0)
    guarantee_len = models.CharField(max_length=10, blank=True, default="وارد نشده")
    delivery_time = models.CharField(max_length=10, blank=True, default="وارد نشده")
    color = MultiSelectField(choices=CARPET_COLORS, null=True)
    size1 = models.CharField(max_length=20, choices=CARPET_SIZES, null=True, blank=True)
    price1 = models.PositiveIntegerField(default=0)
    size2 = models.CharField(max_length=20, choices=CARPET_SIZES, null=True, blank=True)
    price2 = models.PositiveIntegerField(default=0)
    size3 = models.CharField(max_length= 20, choices=CARPET_SIZES, null=True, blank=True)
    price3 = models.PositiveIntegerField(default=0)
    size4 = models.CharField(max_length= 20, choices=CARPET_SIZES, null=True, blank=True)
    price4 = models.PositiveIntegerField(default=0)
    size5 = models.CharField(max_length= 20, choices=CARPET_SIZES, null=True, blank=True)
    price5 = models.PositiveIntegerField(default=0)
    size6 = models.CharField(max_length= 20, choices=CARPET_SIZES, null=True, blank=True)
    price6 = models.PositiveIntegerField(default=0)
    image1 = models.ImageField(null=True, blank=True, upload_to='carpet/images/')
    image2 = models.ImageField(null=True, blank=True, upload_to='carpet/images/')
    image3 = models.ImageField(null=True, blank=True, upload_to='carpet/images/')
    image4 = models.ImageField(null=True, blank=True, upload_to='carpet/images/')
    image5 = models.ImageField(null=True, blank=True, upload_to='carpet/images/')
    image6 = models.ImageField(null=True, blank=True, upload_to='carpet/images/')
    image7 = models.ImageField(null=True, blank=True, upload_to='carpet/images/')
    image8 = models.ImageField(null=True, blank=True, upload_to='carpet/images/')
    admin_featured = models.BooleanField(default=False)
    admin_confirmed = models.BooleanField(default=False)

    def __str__(self):
            return "عنوان: %s / %s" % self.title,self.category.title

    def as_dict(self):
        if self.image1:
            image = self.image1.url
        else:
            image = "بدون تصویر"
        if self.category.category_parent:
            p = self.category.category_parent.title
        else:
            p = 'ندارد'
        return {
            "id": self.id,
            "Title": self.title,
            "Category": self.category.title,
            "Price1": self.price1,
            "Price2": self.price2,
            "Price3": self.price3,
            "Price4": self.price4,
            "Price5": self.price5,
            "Description": self.description,
            "CategoryParent": p,
            "Image": str(image),
            "Confirmed": str(self.admin_confirmed),
            "Featured": str(self.admin_featured),
        }


class TableauRugCategory(models.Model):
    title = models.CharField(max_length=30, default="وارد نشده")
    category_parent = models.ForeignKey('self', blank=True, null=True, on_delete=models.PROTECT)
    image1 = models.ImageField(null=True, upload_to='tableau-category/images/')
    image2 = models.ImageField(null=True, upload_to='tableau-category/images/')

    def __str__(self):
        if self.category_parent:
            return "عنوان: %s / %s" % self.title,self.category_parent.title
        else:
            return "%s" % self.title


class TableauRug(models.Model):

    title = models.CharField(max_length=30, default='وارد نشده')
    description = models.TextField(null=True, blank=True)
    category = models.ForeignKey(TableauRugCategory, null=True, on_delete=models.PROTECT)
    brush_num = models.PositiveIntegerField(default=0)
    guarantee_len = models.CharField(max_length=10, blank=True, default="وارد نشده")
    delivery_time = models.CharField(max_length=10, blank=True, default="وارد نشده")
    weight = models.PositiveIntegerField(default=0)
    size1 = models.CharField(max_length=20, choices=TABLEAURUG_SIZE, null=True, blank=True)
    price1 = models.PositiveIntegerField(default=0)
    size2 = models.CharField(max_length=10, choices=TABLEAURUG_SIZE, null=True, blank=True)
    price2 = models.PositiveIntegerField(default=0)
    size3 = models.CharField(max_length=20, choices=TABLEAURUG_SIZE, null=True, blank=True)
    price3 = models.PositiveIntegerField(default=0)
    size4 = models.CharField(max_length=20, choices=TABLEAURUG_SIZE, null=True, blank=True)
    price4 = models.PositiveIntegerField(default=0)
    size5 = models.CharField(max_length=20, choices=TABLEAURUG_SIZE, null=True, blank=True)
    price5 = models.PositiveIntegerField(default=0)
    size6 = models.CharField(max_length=20, choices=TABLEAURUG_SIZE, null=True, blank=True)
    price6 = models.PositiveIntegerField(default=0)
    image1 = models.ImageField(null=True, blank=True, upload_to='tableau/images/')
    image2 = models.ImageField(null=True, blank=True, upload_to='tableau/images/')
    image3 = models.ImageField(null=True, blank=True, upload_to='tableau/images/')
    image4 = models.ImageField(null=True, blank=True, upload_to='tableau/images/')
    image5 = models.ImageField(null=True, blank=True, upload_to='tableau/images/')
    image6 = models.ImageField(null=True, blank=True, upload_to='tableau/images/')
    image7 = models.ImageField(null=True, blank=True, upload_to='tableau/images/')
    image8 = models.ImageField(null=True, blank=True, upload_to='tableau/images/')
    admin_featured = models.BooleanField(default=False)
    admin_confirmed = models.BooleanField(default=False)


class OrderedItem(models.Model):

    item_carpet = models.ForeignKey(Carpet, null=True, blank=True, on_delete=models.PROTECT)
    item_tableau = models.ForeignKey(TableauRug, null=True, blank=True, on_delete=models.PROTECT)
    ordered_date = models.DateTimeField(auto_now_add=True)
    numbers = models.SmallIntegerField(default=0)
    color = models.CharField(max_length=10, null=True, blank=True)
    price = models.IntegerField(default=0)
    total_price = models.IntegerField(default=0, null=True)

    def __str__(self):
        if self.item_carpet:
            return "%s" % self.item_carpet
        else:
            return "%s" % self.item_tableau


class ShoppingBag(models.Model):

    owner = models.ForeignKey(User, null=True, on_delete=models.PROTECT)
    items = models.ManyToManyField(OrderedItem, blank=True)
    total_price = models.IntegerField(blank=True, default=0)
    draft = models.BooleanField(default=True)
    creation_date = models.DateTimeField(null=True, auto_now_add=True)
    commited_date = models.DateTimeField(null=True, blank=True)
    commtion_tracking_code = models.CharField(max_length=15, blank=True, null=True, default='0')
    cancellation = models.BooleanField(default=False)
    cancellation_date = models.CharField(max_length=15, default="not canceled", blank=True, null=True)
    cancellation_description = models.TextField(default="not canceled", blank=True, null=True)
    prepayment_payed = models.BooleanField(default=False)
    current_step = models.CharField(max_length=20, choices=BAG_STEPS, null=True, default="0")

    def __str__(self):
        return "%s" % self.owner.username


class Complaint(models.Model):

    full_name = models.CharField(max_length = 50, null=True, blank=True)
    phonenumber = models.CharField(max_length=15, null=True, blank=True)
    email = models.EmailField(null=True)
    text = models.TextField(null=True)
    answered = models.BooleanField(default=False)
    creation_date = models.DateTimeField(null=True, auto_now_add=True)

    def __str__(self):
        return "%s" % self.text

    def as_dict(self):
        return {
            "id": self.id,
            "Name": self.full_name,
            "Phonenumber": self.phonenumber,
            "Email": self.email,
            "Text": self.text,
            "Answered": self.answered,
            "Date": str(self.creation_date).split(".")[0],
        }


class Contact(models.Model):

    full_name = models.CharField(max_length = 50, null=True, blank=True)
    phonenumber = models.CharField(max_length=15,null=True,blank=True)
    email = models.EmailField(null=True)
    text = models.TextField(null=True)
    answered = models.BooleanField(default=False)
    creation_date = models.DateTimeField(null=True, auto_now_add=True)

    def __str__(self):
        return "%s" % self.full_name

    def as_dict(self):
        return {
            "id": self.id,
            "Name": self.full_name,
            "Phonenumber": self.phonenumber,
            "Email": self.email,
            "Text": self.text,
            "Answered": self.answered,
            "Date": str(self.creation_date,)
        }


class ContactAnswer(models.Model):
    contact = models.ForeignKey(Contact, blank=True, null=True, on_delete=models.CASCADE)
    subject = models.CharField(max_length=50)
    text = models.TextField(max_length=500)
    creation_date = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return "%s" % self.subject

    def as_dict(self):
        return {
            "id": self.id,
            "Contact": self.contact.text,
            "Subject": self.subject,
            "CreationDate": self.creation_date,
            "Text": self.text,
        }


class ComplaintAnswer(models.Model):
    complaint = models.ForeignKey(Complaint, blank=True, null=True, on_delete=models.CASCADE)
    subject = models.CharField(max_length=50)
    text = models.TextField(max_length=500)
    creation_date = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return "%s" % self.subject

    def as_dict(self):
        return {
            "id": self.id,
            "Complaint": self.complaint.text,
            "Subject": self.subject,
            "CreationDate": self.creation_date,
            "Text": self.text,
        }


class Post(models.Model):
    creation_date=models.DateTimeField(null=True, auto_now_add=True)
    admin_confirmed=models.BooleanField(default=False)
    title = models.CharField(max_length=255, default="",null=True)
    content = models.TextField(null=True,max_length=3000)
    author = models.ForeignKey(User, on_delete=models.CASCADE,null=True)
    category = models.CharField(max_length=20, choices=POST_CATEGORY, default="دسته_بندی_نشده")
    image1 = models.FileField(null=True, upload_to='posts/images/')
    image2 = models.FileField(null=True, blank=True, upload_to='posts/images/')
    image3 = models.FileField(null=True, blank=True, upload_to='posts/images/')
    image4 = models.FileField(null=True, blank=True, upload_to='posts/images/')

    def __str__(self):
        return self.title

    def as_dict(self):
        if self.author:
            user_name=self.author.username
        else:
            user_name=""

        return {
            "id": self.id,
            "Title":self.title,
            "Content":self.content,
            "Creation_date":str(self.creation_date),
            "User":self.author.username,
            "Category":self.category,
            "Confirmed":str(self.admin_confirmed),
        }
    class Meta:
        ordering = ('title', 'creation_date')


